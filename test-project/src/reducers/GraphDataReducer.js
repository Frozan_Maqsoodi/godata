import {
    GET_DATA_REQUESTED,
    GET_DATA_SUCCEEDED,
    GET_DATA_FAILED,
} from '../actions/index.js';

const initialState = {
    fetchState: {inFlight: false},
    data: []
  };

const graphDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_DATA_REQUESTED: {
            return {...state,
            fetchState:{ inFlight: true}
        };
        }

        case GET_DATA_SUCCEEDED: {
            return {...state, 
            fetchState: {inFlight: false},
            data: action.payload
        };
        }

        case GET_DATA_FAILED: {
            return {...state,
            fetchState: {inFlight: false, error: action.payload},
        };
        }
        default:
            return state;
    }
};

export default graphDataReducer;
