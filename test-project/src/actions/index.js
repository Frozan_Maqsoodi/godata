export const GET_DATA_REQUESTED = 'GET_DATA_REQUESTED';
export const getDataRequested = () => ({type: GET_DATA_REQUESTED});

export const GET_DATA_SUCCEEDED = 'GET_DATA_SUCCEEDED';
export const getDataSucceeded = (data) => ({
  type: GET_DATA_SUCCEEDED,
  payload: data
});

export const GET_DATA_FAILED = 'GET_DATA_FAILED';
export const getDataFailed = (reason) => ({
  type: GET_DATA_FAILED,
  payload: reason
});
