import {
    getDataRequested,
    getDataFailed,
    getDataSucceeded,
} from './index';
import axios from 'axios';

const SERVER_ADDRESS = 'http://127.0.0.1:5000';
export const getData = (fetch = window.fetch) => {
      // debugger;
  return (dispatch) => {
    dispatch(getDataRequested());
    axios.get(SERVER_ADDRESS + '/api/forecasts')
      .then(function (response) {
        console.log('>>>', response.data)
        dispatch(getDataSucceeded(response.data))
      })
      .catch(function (error) {
        dispatch(getDataFailed('Service unreachable'));
          console.log(error);
      });
  };
}
// export const getData = (fetch = window.fetch) => {
//   // debugger;
//     return (dispatch) => {
//       dispatch(getDataRequested());
//       return fetch(SERVER_ADDRESS + '/api/forecasts')
//         .then((response) => {
//           if (response.ok) {
//             response.json().then(
//               (data) => dispatch(getDataSucceeded(data)),
//               (error) => dispatch(getDataFailed('Unparseable response'))
//             );
//           } else {
//             response.json().then(
//               ({error}) => dispatch(getDataFailed(error)),
//               (error) => dispatch(getDataFailed('Unparseable response'))
//             );
//           }
//         }).catch((error) => {
//           dispatch(dispatch(getDataFailed('Service unreachable')));
//         });
//     };
//   };