import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reducer from './reducers';
import {createStore, compose, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import App from '../src/containers/App';


const composeStoreEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

let store = createStore(
    reducer,
    composeStoreEnhancers(
      applyMiddleware(
        thunk,
        logger
      )
    )
  );

ReactDOM.render(
    <Provider store={store}>
    <App />
    </Provider>,
    document.getElementById('root')
);
