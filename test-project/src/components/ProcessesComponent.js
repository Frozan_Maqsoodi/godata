import React, { Component } from 'react';

import Dock from '../../node_modules/react-dock';
import GraphComponent from './GraphComponent';
import FilteringAttributes from './FilteringAttributes';

class ProcessesComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
          isVisible: true,
          contentVisible: false,
          dimMode: 'none'
        }
        
      }
  
    render() {
        return (
            <div>
                <div onClick={() => this.setState({ isVisible: !this.state.isVisible })}>Maximize</div>
                <GraphComponent/>
                <Dock className = "dock" position='bottom' isVisible={this.state.isVisible} dimMode={this.state.dimMode} >
                    <div onClick={() => this.setState({ isVisible: !this.state.isVisible })}>Minimize</div>                
                    <div>
                    <FilteringAttributes/>
                    </div>
                </Dock>
            </div>
        );
    }
}

export default ProcessesComponent;
