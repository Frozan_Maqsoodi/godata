import React, { Component } from 'react';
import * as d3 from 'd3';
import '../css/styles.css';
import { connect } from 'react-redux';
import {getData} from '../actions/DataServerActions';
import PropTypes from 'prop-types';

class ForecastsGraph extends Component {
    constructor(props) {
        super(props)
        this.state = { data: []};
    }

    componentDidMount() {
        this.setState({data: this.props.data});
        this.props.getData(this.props.data.data);
        console.log('this is setState', this.state.data);
        console.log('>>>componentDidMount-state',this.props.data);
        console.log('>>>componentDidMount-props',this.props.data.data);      
    }
    // componentWillReceiveProps(nextProps) {
    //     this.setState({data: nextProps.data});
    //     console.log('compwillRecvProps', nextProps.data);
    // }

    // componentWillReceiveProps(newProps) {
        // if (this.state.data !== newProps.data) {
        //   this.setState({data: newProps.data});
        //   console.log('this.state',this.state.data);
        // }
        // console.log('componentWillReceiveProps',newProps.data);
    //   }
    componentDidUpdate(prevProps) {
        console.log("----");
        console.log(this.props.data);
        if (this.props.data.forecasts) {
            var data = [];
            Object.entries(this.props.data.forecasts)
                  .forEach(([k,value]) => data.push(Object.assign({"days": k}, value)));
            this.draw(data);
        }
    //     if (this.props.data.data !== prevProps.data.data) {
    //         if (this.props.data && this.props.data.forecasts) {
    //             this.draw(this.props.data.data);
    //         }
    //         console.log('>>>componentDidUpdate', this.props.data.data)
    //         console.log(',,,,,', this.props.data.data)
        }
       
    // }

    draw(data) {
        console.log('data>>>', this.state.data);
        // set the dimensions and margins of the graph
        var svg = d3.select("svg"),
            margin = {top: 20, right: 20, bottom: 200, left: 40},
            margin2 = {top: 430, right: 20, bottom: 30, left: 40},
            width = +svg.attr("width") - margin.left - margin.right,
            height = +svg.attr("height") - margin.top - margin.bottom,
            height2 = +svg.attr("height") - margin2.top - margin2.bottom;
            console.log('this is height', height)
        // set scales 
        var x = d3.scaleLinear().range([0, width]),
            x2 = d3.scaleLinear().range([0, width]),
            y = d3.scaleLinear().range([height, 90]),
            y2 = d3.scaleLinear().range([height2, 0]);


        var xAxis = d3.axisBottom(x),
            xAxis2 = d3.axisBottom(x2),
            yAxis = d3.axisLeft(y);

        // var brush = d3.brushX()
        //     .extent([[0, 0], [width, height2]])
        //     .on("brush end", brushed);

        // var zoom = d3.zoom()
        //     .scaleExtent([1, Infinity])
        //     .translateExtent([[0, 0], [width, height]])
        //     .extent([[0, 0], [width, height]])
        //     .on("zoom", zoomed);

        // define the line
        var valueline = d3.line()
            .curve(d3.curveMonotoneX)
            .x(function(d, i) { return x(i); })
            .y(function(d) { return y(d.forecast); });

        // define the line
        var valueline2 = d3.line()
            .curve(d3.curveMonotoneX)
            .x(function(d, i) { return x(i); })
            .y(function(d) { return y(d.lower); });
            
        // define the line
        var valueline3 = d3.line()
            .curve(d3.curveMonotoneX)
            .x(function(d, i) { return x(i); })
            .y(function(d) { return y(d.upper); });

        // var valueline4 = d3.line()
        //     .curve(d3.curveMonotoneX)
        //     .x(function(d, i) { return x2(i); })
        //     .y(function(d) { return y2(d.forecast); });

        // var valueline5 = d3.line()
        //     .curve(d3.curveMonotoneX)
        //     .x(function(d, i) { return x2(i); })
        //     .y(function(d) { return y2(d.upper); });

        // var valueline6 = d3.line()
        //     .curve(d3.curveMonotoneX)
        //     .x(function(d, i) { return x2(i); })
        //     .y(function(d) { return y2(d.lower); });
        x.domain([0, data.length-1]);
        y.domain([0, d3.max(data, 
            function(d) {return d.lower; },
            function(d) {return d.upper;},
            function(d) {return d.forecast;}
        )
        ]);
        x2.domain(x.domain());
        y2.domain(y.domain());

        // read the data
        [data].forEach(function(d) {
            d.forecast = +d.forecast;
            d.upper = +d.upper;
            d.lower = +d.lower;
        });

        svg.append("defs").append("clipPath")
            .attr("id","clip")
            .append("rect")
            .attr("width", width)
            .attr("height", height);

        var focus = svg.append("g")
            .attr("class", "focus")
            .attr("transform", 
                  "translate(" + margin.left + "," + margin.top + ")")

        var context = svg.append("g")
            .attr("class", "context")
            .attr("transform", 
                 "translate(" + margin2.left + "," + margin2.top + ")")  

        focus.append("g")
        .data([data])
        .attr("class", "axis axis--x")
        .attr("transform", "translate(0," + height + ")")
        .attr("class", "line")
        // .attr("d", valueline)
        .call(xAxis);

        focus.append("g")
            .attr("class", "axis axis--y")
            .call(yAxis);
            
        // context.append("g")
        //     .attr("class", "axis axis--x")
        //     .attr("transform", "translate(0," + height2 + ")")
        //     .call(xAxis2);
        
        // context.append("g")
        //     .attr("class", "brush")
        //     .call(brush)
        //     .call(brush.move, x.range());
    
        // context.append("rect")
        //     .attr("class", "zoom")
        //     .attr("width", width)
        //     .attr("height", height)
        //     .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
        //     .call(zoom);

        //************************** 
        var tooltip = d3.select("body").append("div").attr("class", "toolTip");
        // // Add the valueline path.
        focus.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline)
            .style("stroke", "red")

        // // Add the valueline path.
        focus.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline2)
            .style("stroke", "blue") 

        // // Add the valueline path.
        focus.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline3)
            .style("stroke", "green")
            
        focus.selectAll("dot")
            .data(data)
            .enter()
            .append('circle')
                .attr('r', 2)
                .attr('cx', function(d,i) { return x(i); })
                .attr('cy', function(d) {return y(d.lower); })
            //attach mouse hover behaviour to the dots
            .on("mouseover", function(d) {
                tooltip
                  .style("left", d3.event.pageX - 50 + "px")
                  .style("top", d3.event.pageY - 70 + "px")
                  .style("display", "inline-block")
                  .html("<div>Lower: <br/>" + (d.lower) + "</div>" );
              });

        focus.selectAll("dot")
            .data(data)
            .enter()
            .append('circle')
                .attr('r', 2)
                .attr('cx', function(d,i) { return x(i); })
                .attr('cy', function(d) {return y(d.upper); })
            //attach mouse hover behaviour to the dots
            .on("mouseover", function(d) {
                tooltip
                .style("left", d3.event.pageX - 50 + "px")
                .style("top", d3.event.pageY - 70 + "px")
                .style("display", "inline-block")
                .html("<div>Upper: <br/>" + (d.upper) + "</div>" );
            })

        focus.selectAll("dot")
            .data(data)
            .enter()
            .append('circle')
                .attr('r', 2)
                .attr('cx', function(d,i) { return x(i); })
                .attr('cy', function(d) {return y(d.forecast); })
            //attach mouse hover behaviour to the dots
            .on("mouseover", function(d) {
                tooltip
                    .style("left", d3.event.pageX - 50 + "px")
                    .style("top", d3.event.pageY - 70 + "px")
                    .style("display", "inline-block")
                    .html("<div>Forecast: <br/>" + (d.forecast) + "</div>" );
                }); 
        //************************* */
    
        // function brushed() {
        //     if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom") return; // ignore brush-by-zoom
        //     console.log("Brushed is called");
        //     var s = d3.event.selection || x2.range();
        //     x.domain(s.map(x2.invert, x2));
        //     focus.selectAll(".line")
        //     .attr("d", valueline)
        //     focus.selectAll(".axis--x")
        //         .call(xAxis);
        //     svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
        //         .scale(width / (s[1] - s[0]))
        //         .translate(-s[0], 0));
        // }
            
        // function zoomed() {
        //     if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush") return; // ignore zoom-by-brush
        //     console.log("Zoom is called");
        //     var t = d3.event.transform;
        //     x.domain(t.rescaleX(x).domain());
        //     focus.select(".line")
        //          .attr("d", valueline)
        //     focus.select(".axis--x").call(xAxis);
        //     context.select(".brush").call(brush.move, x.range().map(t.invertX, t));
        // } 
}

render() {
        return (
            <div className="chart" >
                <svg width="960" height="500"></svg>
                {/* <GraphComponent data={this.state.data} /> */}
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    data: state.data.data
})

ForecastsGraph.propTypes = {
    getData: PropTypes.func.isRequired,
    data: PropTypes.node.isRequired
}

export default connect(mapStateToProps, {getData}) (ForecastsGraph);
