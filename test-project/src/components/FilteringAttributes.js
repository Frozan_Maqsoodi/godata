import React, { Component } from 'react';

import {CheckboxSelect} from '@atlaskit/select';
import FieldRadioGroup from '@atlaskit/field-radio-group';
import '../css/Attributes.css';

class FilteringAttributes extends Component {
    // constructor(props) {
    //     super(props);
        
    // }
    
      onRadioChange = (event = this.any) => {
        console.log(event.target.value);
      };

    render() {

        return (
            <div>
                <CheckboxSelect
                    className="checkbox-select"
                    classNamePrefix="select"
                    onSelectChange = {this.onSelectChange}
                    options={[
                        {label: 'Motorcycle', value: 'motorCycle'},
                        {label: 'Car', value: 'car'},
                        {label: 'Car with goods', value: 'carGoods'},
                        {label: 'Truck with goods', value: 'truck'},
                        {label: 'Bus', value: 'bus'},
                        {label: 'Empty Truck', value: 'emptyTruck'}
                    ]}
                        placeholder="Select Vehicle"
                />
                <CheckboxSelect 
                    className="checkbox-select"
                    classNamePrefix="select"
                    options={[
                        {label: 'EU', value: 'eu'},
                        {label: 'Estonian', value: 'estonian'},
                        {label: 'Russian', value: 'russian'}
                    ]}
                        placeholder="Select Country"
                />
                 <CheckboxSelect 
                    className="checkbox-select"
                    classNamePrefix="select"
                    options={[
                        {label: 'Reservation', value: 'reservation'},
                        {label: 'Live', value: 'live'}
                    ]}
                    placeholder="Select Queue"
                />
                 <FieldRadioGroup className = "field-radio-group"
                items= {[
                    { value: 'approved', label: 'Approved', defaultSelected: true },
                    { value: 'pending', label: 'Pending' }
                  ]}
                  label = {'Select Status'}
                  onRadioChange={this.onRadioChange}
                />
            </div>
        )
    }
}

export default FilteringAttributes;