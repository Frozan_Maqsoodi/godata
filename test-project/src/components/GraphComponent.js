import React, { Component } from 'react';
import ForecastsGraph from './ForecastsGraph';

class GraphComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            forecasts: []
        }   
    }

render(){
    return (
        <div>
             <h3>Forecasts Line Graph</h3>
            <ForecastsGraph forecasts = {this.state.forecasts} />
            
        </div>
    )

}

}

export default GraphComponent;
